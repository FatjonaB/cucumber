package internship.global;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class GlobalVariables {

    public static final String APP_URL = "https://demo.nopcommerce.com/";
    public static final String GENDER = "F";
    public static final String FIRSTNAME ="Fatjona";
    public static final String LASTNAME ="Bucpapaj";
    public static final String BIRTH_DAY ="06";
    public static final String BIRTH_MONTH ="April";
    public static final String BIRTH_YEAR ="1998";
    public static final String EMAIL ="fatjona@yahoo.com";
    public static final String COMPANY_NAME ="lufthansa";
    public static final String PASSWORD ="fatjona1234";
    public static final String LOGIN_RESULT = "Welcome to our store";
    public static final String WELCOME = "Welcome, Please Sign In!";
    public static final String RESULT = "Your registration completed";
    public static final String TITTLE_ACCOUNT_PAGE = "nopCommerce demo store. Account";
    public static final String TITTLE_NOTEBOOK_PAGE = "nopCommerce demo store. Notebooks";
    public static final String SHOPPING_CART_SUCCESS_MESSAGE = "The product has been added to your shopping cart";
    public static final String WISHLIST_SUCCESS_MESSAGE = "The product has been added to your wishlist";
}
